package net.seajay.nfsandwiches;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;


@Mod (modid = "nfsandwiches", name="Need for Sandwiches", version = "1.0")
public class NFSandwiches {
	public static Item DaisiesSandwich;
	@EventHandler
	public void preLoad(FMLPreInitializationEvent event)
	{
		DaisiesSandwich = new ItemDaisiesSandwich(2, 1.3f, true).setPotionEffect(Potion.regeneration.id, 5, 0, 0.7F).setUnlocalizedName("DaisiesSandwich");
		GameRegistry.registerItem(DaisiesSandwich, "DaisiesSandwich");
		GameRegistry.addShapelessRecipe(new ItemStack(NFSandwiches.DaisiesSandwich, 4), new Object[] {Items.bread, new ItemStack(Blocks.red_flower, 1, 8), new ItemStack(Blocks.red_flower, 1, 8)});
	}
}
