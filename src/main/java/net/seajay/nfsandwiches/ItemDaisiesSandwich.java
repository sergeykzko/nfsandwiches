package net.seajay.nfsandwiches;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;

public class ItemDaisiesSandwich extends ItemFood {

	public ItemDaisiesSandwich(int healAmount, float saturationModifier, boolean wolvesFavorite) {
		super(healAmount, saturationModifier, wolvesFavorite);
		this.setCreativeTab(CreativeTabs.tabFood);
		this.setTextureName("NFSandwiches:DaisiesSandwich");
		//this.setAlwaysEdible();
	}
};
